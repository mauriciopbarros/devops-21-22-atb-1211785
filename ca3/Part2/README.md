# Class Assignment 3 Report - Part 2

### Virtualization with Vagrant

## 1. Analysis, Design and Implementation

### **Use Vagrant to setup a virtual environment**

The objective for this assignment was to setup a virtual environment using vagrant in order to execute the application previously developed in ca2. For this part I only copied what is inside the folder named
*react-and-spring-data-rest-basic* which is in the folder *Gradle_Project* in the CA2-Part2.
Also for the app to work i had to configure the vagrant file. 

#### New VM in 3 Steps 

* **STEP 1:**

This first step was to create a folder where I initialized a vagrant project,
for this I used the following command,

>mkdir vagrant-project-1

then, moved inside the new directory

* **STEP 2:**

Then, I had to create a new Vagrant configuration file for the project
, for this I used the following command, 

>vagrant init bento/ubuntu-18.04

* **STEP 3:**

For the third and last step, it was time to start the VM,
so I just had to type the command,

>vagrant up


Then, and just to be sure how was the status of the virtual environment I typed,

>vagrant status

Since the VM was running properly it was time to start the ssh session with the command : 

>vagrant ssh 

#### 1.1.1 Clone from bitbucket 

The first step was to use https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/
as an initial solution. So i cloned to my computer. 

> git clone https://MauricioPBarros@bitbucket.org/atb/vagrant-multi-spring-tut-demo.git

#### 1.1.2 Vagrantfile

After analysing the vagrant file i understood that it supposed to be used to create to virtual machines , one to run the web server and another one to execute the database server. 
* **web**: this VM is used to run tomcat and the spring boot basic application
* **db**: this VM is used to execute the H2 server database

#### 1.1.3 Copy Vagrantfile into repository

As the title says. 

#### 1.1.4 Update Vagrantfile configuration

Since i had some problems during this next part (specially with windows shell), i proceeded to finish the assigment on other OS , linux ubuntu 18.04. 

First I needed to change the Repository Settings to public before running the vagrant file. 
Because of that i needed to update the vagrant file also. More spcecificaly the command to the repository. 

#### 1.1.5 Replicate changes in my version of the spring application

Next step was to check the teacher's repository to see the changes necessary so that the spring application uses the H2 server in the db VM.
After replicating these changes to my app, I inserted the command,

>vagrant up

and with this I created the db and web vm's.
I had to .gitignore the .vagrant folder in order to not make it occupy a lot of space in my remote repository.

Then checked if the two machines were running, and to see this I had to use the command,

>vagrant status

Now, I checked if everything was working with each one of them.

**First, let's start with the **db**:**

I ran the command,

> vagrant ssh db

to start the Virtual Machine.
Then, to see if the H2 server database was being executed I ran the command,

> ps -xa | grep h2


Last, I had to check with the URL's if everything was working correctly with the **db**.

Open the H2 console using one of the following urls:

* http://localhost:8080/basic-0.0.1-SNAPSHOT/h2-console
* http://192.168.56.10:8080/basic-0.0.1-SNAPSHOT/h2-console


**Now, let's work with the **web**:**

I ran the command,

> vagrant ssh web

to start the Virtual Machine.
Then, to see if tomcat was being executed I ran the command,

> ps -xa | grep tomcat8

Just like with the ***db*** VM I had to check with the URL's if everything was working correctly with the **db**.

On the host I opened the spring web application using one of the following options:

* http://localhost:8080/basic-0.0.1-SNAPSHOT/
* http://192.168.56.10:8080/basic-0.0.1-SNAPSHOT/


