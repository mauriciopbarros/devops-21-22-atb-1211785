# Class Assignment 3 Report - Part 1

### Virtualization with Vagrant

## 1. Analysis, Design and Implementation

Because the majority of this assigment was supported and done by the professor on the lecture I'll describe step by step in a very sucint way. 

### 1.1 Create a VM with ubuntu

After installing  Virtual Box, i configured the VM acordingly to the teacher's indications. 
I then proceeded to instal ubuntu on it. Its worth to refer that this was a "mini" version of ubuntu, without any interface. 

### 1.2 Clone your individual repository inside the VM

Simple clone of my repo to the Virtual Machine.

### 1.3 Build and Execute the spring boot tutorial basic project and the gradle_basic_demo project (from the previous assignments)


Then I built the application using the command:

    ./mvnw spring-boot:run


Then, and as needed I installed the dependencies that were necessary for the project.

First, I installed maven:

    sudo apt install maven


Then, gradle:

    sudo apt install gradle


#### 1.3.1. Previous Assignments : Build CA1

After installing the dependencies it was time to build and run the first Class Assignment inside the VM and
see if everything was working as expected. For this I run the following command:

    mvn spring-boot:run


#### 1.3.2. Previous Assignments : Build CA2

#### 1.3.2.1 CA2 Part One:

For the second Class Assignment I divided what I did into two parts as it was mandatory to do previously.
Both these parts were dedicated to using gradle and building a project using this.

First, I used the command:

        ./gradlew build

Secondly, in my VM I run the server with the command:

        ./gradlew runServer


Since for this task a interface/display was needed I needed to create a new gradle task to launch the chat client as shown bellow. 

    task runClientVM(type:JavaExec, dependsOn: classes){

        group = "DevOps"
        description = "Launches a chat client that connects to a server on 192.168.56.5 "

        classpath = sourceSets.main.runtimeClasspath

        main = 'basic_demo.ChatClientApp'

            args '192.168.56.5', '59001'
    }

I think its worth to mention that i did  lost a lot of time in this part because of a very silly mistake. Basically, i wasnt running the service before running the client , so it took me a while to understand that. 


#### 1.3.2.2 CA2 Part Two:

For the second part of CA2 I ran, as usual, the command

      ./gradlew build


and everything worked successfully,

After the build, I verified that the folder built had two files in it,
one was the bundle.js and the other was bundle.js.map.


To empty the folder built I ran the task I created for the CA2:

    task deleteFilesGeneratedByWebpack (type: Delete){
        delete fileTree('src/main/resources/static/built')
    }

To end this first part of the Class Assignment 3 I after adding all the modifications i ended all issue and tagged it with "ca3-part1".








