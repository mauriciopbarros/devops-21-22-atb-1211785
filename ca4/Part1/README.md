# Class Assignment 4 Report - Part 1

### **Containers with Docker**

## 1. Analysis, Design and Implementation

The first step was to install Docker in my computer. 
This time because of some problems in my Linux OS i decided to try windows. I proceeded to install the latest docker version. 

### 1.1 Create docker images and running containers using the chat application from CA2

#### **1.1.1. Create a docker image to execute the chat server:**

To start the assignment, I created a dockerfile with the propper configurations. Configurations like the correct directory, and, in this case, my repository. 
A dockerfile it's a text document which contains the commands for assembling an image.

* **Chat server built with the Dockerfile:**

In order to complete this task, builduing the app inside the container, the dockerfile was configured like we can see bellow: 

![dockerfilescreenshot](../Part1/Screenshots/dockerfilescreenshot.jpg)

After setting all of this it's time to start the docker container.

**Starting the docker container**

In the dockerFile directory to build the docker container we run the following command: 

> docker build . -t chat_docker_v1

After the build is complete, we start the container, using the following command,

> docker run -p 59001:59001 -d chat_docker_v1

Here, the communication ports for the host and guest were also defined in the port 59001.
And as can be seen below everything worked as expected.

![DockerRunning](/ca4/Part1/Screenshots/cmdAndDockerRunning.jpg)

Then, to start the app :

> ./gradlew runClient 

![chatServerRunning](../Part1/Screenshots/chatRunning.jpg)

Also, in the Docker app :

![chatServerRunningDocker](../Part1/Screenshots/chatRunningOnDocker.jpg)

**Chat server built in the Host Computer and copy the jar file "into" the Dockerfile:**

In the second part of this assignment we're supposed to copy the jar file that we generated into the docker file in order to run the app through the host computer. 
First, I had to stop the running container created for the first part, I did this using the Docker Desktop application.

Next, I created a new folder inside the ca4-Part1 folder, named "v2".
and copied the executable jar file generated with the built I did of my gradle_basic_demo found in my repository.
Here I also added a new Dockerfile. 

After this, just like in the previous version I ran the commands to build and run the docker. 

> docker build . -t chat_docker_v2
> docker run -p 59001:59001 -d chat_docker_v2

![v2DockerBuild](../Part1/Screenshots/v2DockerBuild.jpg)

the app running : 

![appRunning](../Part1/Screenshots/chatServerDockerV2.png)

### 1.2 Tag the image and publish it in docker hub

After creating an acount at dockerhub, to complete the assignment i then created and published the repository. 

to check my images: 

> docker image ls 

![imagesDocker](../Part1/Screenshots/dockerImages.png)

Tagging and pushing the image to the repo : 

![pushedTag](../Part1/Screenshots/pushedTag.png)

DockerHub profile after tasks completion : 

![latestPUSH](../Part1/Screenshots/latestPUSH.png)

and, 

![latestTAG](../Part1/Screenshots/latestTAG.png)

