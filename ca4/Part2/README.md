# Class Assignment 4 Report - Part 2

## 1. Analysis, Design and Implementation

For this assignment we're supposed to use Docker to setup a container environment to execute the app from the previous assignments.
The goal is to use the Docker compose on two different containers , web and db. 

#### 1.1 Use docker-compose to produce 2 services/containers

For this part, I started to create the docker-compose file accordingly to the teacher's version. Also, two docker files , web and  db, were created in different folders.

**Docker-Compose**

![docker-composeFile](../Part2/Screenshots/DockerFileCompose.png)

The only change I had to make for this file was the ipv4 and it's subnet address to the ones bellow: 

**"192.168.56.11"** 
**"192.168.56.0/24"**

**DockerFile Web**

![docker-web](../Part2/Screenshots/DockerFileWeb.png)

Its also worth to say that in every single one of these files, to avoid problems, I also changed the JDK version from 8 to 11. 
After inserting my repo clone i also added the command "chmod u+x gradlew" to get the needed permissions while building it. 

**DockerFile Db**

![docker-db](../Part2/Screenshots/DockerFileDB.png)

After all of this it is time to build with the docker-compose file. After building successfully I ran the command docker compose up. 

![dockerComposeBuild](../Part2/Screenshots/FinishedBUILDCompose.png)

![dockerComposeUp](../Part2/Screenshots/DockerComposeUP.png)

As we can see with the screenshots bellow after some grinding all went fine. The Db and web. 

![DBWorking](../Part2/Screenshots/webWorking.png)

![DBWorking](../Part2/Screenshots/webLOcalWorking.png)

#### 1.2 Publish the images (db and web) to Docker Hub

Since the task was to publish both of my images to the DockerHub repository , I started by logging in to my DockerHub account through the terminal in order to get all the necessary permissions to push. 
I then , on my account, created a new repo called "ca4_part2". 
Then, i proceeded to tag and push both images. To do that i used the following commands: 

> **sudo docker tag part2_db mauravilhoso/ca4_part2:part2_db**

> **sudo docker push mauravilhoso/ca4_part2:part2_db**

> **sudo docker tag part2_web mauravilhoso/ca4_part2:part2_web**

> **sudo docker mauravilhoso/ca4_part2:part2_web**

Bellow, both images being tagged and pushed. Also, my repo updated. 

![pushTagForDb](../Part2/Screenshots/dbTAG.png)

![pushTagForWeb](../Part2/Screenshots/webimage.png)

Finnaly , my updated Repository:

![updatedRepository](../Part2/Screenshots/REPOUpdatedWithImages.png)


#### 1.3 Use a volume with the db container

The goal int this last task for this part was to use with the db container to get a copy of the database through the terminal. 
First, i listed the containers: 

> **docker ps -a**

then executed it : 

> **sudo docker exec -it 09f0c4ad2f0a bash**

Inside the DB, after listing its contents (ls -la) i got the one i needed, jpadb.mv.db and moved it to the directory path that made it acessible to my host machine. 

![fileInData](../Part2/Screenshots/FileInData.png)


> **cp ./jpadb.mv.db /usr/src/data**

With this completed i closed this part and respective issues. 
