# DevOps Technical Report Ca2

##### Maurício Pinto Barros 1112785

## 1. Analysis, Design and Implementation

#### 1.1 Goals/Requirements 


I started by creating as request a new folder for the Part One of CA2.

Then download the content of the repository [here](https://bitbucket.org/luisnogueira/gradle_basic_demo/src/master/)
to the new folder

Doing this, I made the mistake of not deleting the .git folder right away. That actually caused me many problems since my git now, had two different heads. It turned out to be a very instructive mistake since I had to try several solutions. After several attempts and interacting with the professor , the second head was removed by using **git rm --cached** and the problem was solved. 


#### 1.2 Add a new task to execute the server.

To add a new task in case the runServer task, I went to build.gradle file on my IDE and added the next code

    task runServer(type:JavaExec, dependsOn: classes){

    group = "DevOps"

    description = "Launches a server"

    classpath = sourceSets.main.runtimeClasspath

    main = 'basic_demo.ChatServerApp'

    args '59001'
    }

then to see if everything was ok, on terminal I use the command

    ./gradlew runServer

and as we can see in the next image everything was ok.

![runServer](imagesCa2/gnome-shell-screenshot-3QGIK1.png)

All of this was committed resolving issue #7. 

#### 1.4 Add a unit test

First I created the folder to match the path indicated for the assignment.

Then I use the next code to create a test

    package basic_demo;
    import org.junit.jupiter.api.Test;
    import static org.testng.AssertJUnit.assertNotNull;

    public class AppTest {
    @Test
    public void testAppHasAGreeting() {
    App classUnderTest = new App();
    assertNotNull("app should have a greeting", classUnderTest.getGreeting());
    }
    }

After that, I checked if everything was ok:

![appTest](imagesCa2/appTEst.png)
![BuildTest](imagesCa2/buildTest.png)

I finish this task with a new commit and resolving the issue #8

#### 1.5 Add a new task of type Copy

I created a new task in gradle.build

    task backup(type:Copy){
    from 'src'
    into 'backup'
    }

In terminal run

    ./gradle backup

![backup](imagesCa2/gradleBackup.png)

Finally, committed the changes and resolve the issue #9

#### 1.6 Add a new task of type Zip

I created a new task in gradle.build

    task zipBackup(type: Zip) {
    archiveFileName = "srcZipBackup.zip"
    destinationDirectory = file("backup/zips")
    from "src"
    }

In terminal run

    ./gradle zipBackup

![Zip](imagesCa2/zipBack.png)

and now I commit the changes made resolving issue #10.

Part 1 end.

