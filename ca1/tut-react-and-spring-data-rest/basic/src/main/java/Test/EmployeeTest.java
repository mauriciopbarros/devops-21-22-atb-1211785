package Test;

import com.greglturnquist.payroll.Employee;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.testng.AssertJUnit.assertTrue;

class EmployeeTest {

    @Test
    void employeeCreationIsSuccessfully() {
        //Arrange
        String firstName = "Mauravilhoso";
        String lastName = "Barros";
        String description = "git Gud";
        String jobTitle = "Wizard";
        int jobYears = 1;
        String email = "mauravilhoso@isep.ipp.pt";


        //Act
        Employee employee = new Employee(firstName, lastName, description, jobTitle, jobYears,email);
        Employee employee1 = new Employee(firstName, lastName, description, jobTitle, jobYears,email);

        //Assert
        assertEquals(employee, employee1);

    }

    @Test
    void employeeCreationIsUnsuccessfullyJobYearsNegative() {
        //Arrange
        String firstName = "Mauravilhoso";
        String lastName = "Barros";
        String description = "git_Gud";
        String jobTitle = "Wizard";
        int jobYears = -1;
        String email = "mauravilhoso@isep.ipp.pt";


        //Assert
        assertThrows(NullPointerException.class, () ->
                new Employee(firstName, lastName, description, jobTitle, jobYears,email));

    }

    @Test
    void employeeCreationIsUnsuccessfullyFirstNameNull() {
        //Arrange
        String firstName = null;
        String lastName = "Barros";
        String description = "git_Gud";
        String jobTitle = "Wizard";
        int jobYears = 1;
        String email = "mauravilhoso@isep.ipp.pt";


        //Assert
        assertThrows(NullPointerException.class, () ->
                new Employee(firstName, lastName, description, jobTitle, jobYears,email));

    }

    @Test
    void employeeCreationIsUnsuccessfullyLastNameNull() {
        //Arrange
        String firstName = "Mauravilhoso";
        String lastName = null;
        String description = "git_Gud";
        String jobTitle = "Wizard";
        int jobYears = 1;
        String email = "mauravilhoso@isep.ipp.pt";


        //Assert
        assertThrows(NullPointerException.class, () ->
                new Employee(firstName, lastName, description, jobTitle, jobYears,email));

    }

    @Test
    void employeeCreationIsUnsuccessfullyDescriptionNull() {
        //Arrange
        String firstName = "Mauravilhoso";
        String lastName = "Barros";
        String description = null;
        String jobTitle = "Wizard";
        int jobYears = 1;
        String email = "mauravilhoso@isep.ipp.pt";


        //Assert
        assertThrows(NullPointerException.class, () ->
                new Employee(firstName, lastName, description, jobTitle, jobYears,email));

    }

    @Test
    void employeeCreationIsUnsuccessfullyJobTitleNull() {
        //Arrange
        String firstName = "Mauravilhoso";
        String lastName = "Barros";
        String description = "git_Gud";
        String jobTitle = null;
        int jobYears = 1;
        String email = "mauravilhoso@isep.ipp.pt";


        //Assert
        assertThrows(NullPointerException.class, () ->
                new Employee(firstName, lastName, description, jobTitle, jobYears,email));

    }

    @Test
    void employeeCreationIsUnsuccessfullyEmailNull() {
        //Arrange
        String firstName = "Mauravilhoso";
        String lastName = "Barros";
        String description = "git_Gud";
        String jobTitle = "Wizard";
        int jobYears = 1;
        String email = "mauravilhosoisep.ipp.pt";

        //Assert
        assertThrows(NullPointerException.class, () ->
                new Employee(firstName,lastName,description,jobTitle,jobYears, email));

    }

    @Test
    void isEmailValid() {

        //Arrange

        String email = "mauravilhoso@isep.ipp.pt";

        //Act

        boolean result = Employee.emailIsValid(email);

        //Assert

        assertTrue(result);
    }

    @Test
    void isEmailIsInvalid() {

        //Arrange

        String email = "mauravilhosoisep.ipp.pt";

        //Act

        boolean result = Employee.emailIsValid(email);

        //Assert

        assertFalse(result);
    }
}

