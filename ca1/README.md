# DevOps Technical Report

##### Maurício Pinto Barros 1112785

## 1. Analysis, Design and Implementation

In this report I will describe step by step the analysis, design and implementations done on the course of this assignment. During this work I worked on a Linux Ubuntu 20.04 and mainly used the terminal to work on git, firefox and InteliJ for development.

### 1.1 First assignment

We start with the first class where the professor asked us to clone an example repository.  [ProfessorReposotory](https://bitbucket.org/atb/devops-19-20-rep-template/src/master/ca1/)

in order to duplicate the repository I used the git command on terminal 

>**git clone https://MauricioPBarros@bitbucket.org/atb/devops-21-rep-template.git**

after that i pushed it into teh repository with the command 

>**git push**

To end the first assignment I then added , as asked, the job title to the Employee and opened it on the browser and verified that everything worked fine. 

After creating a new folder and moving the tut folder to it (ca1), I committed and pushed the new location with the tag v1.1.0(initial version). To do that the following commands were used: 
>**git add .** (the dot to select all changes)

>**git commit -m "Moved directory tut-react-and-spring-data to CA1 Folder"** (the -m to display insert the commit message)

>**git tag -a v1.1.0**

>**git push origin master v1.1.0**

>**git push --tags**

### 1.1.2 First assignment - Develop a new feature to add a new field to the application

During this task , many commits were done to add a new employee supported by the necessary tests.
After all the updates and due pushes of them the app now supports a new field properly tested.
To run the app I then used the command:

>**./mvnw spring-boot:run**

We can check the results at our localhost:

> https://localhost:8080/


Bellow a preview of that implementation with the application already running:

![NewFeature](tut-react-and-spring-data-rest/basic/images/newEmployee.png)

### 1.1.3 First assignment - Debug the server and client parts

In order to test and check the behaviour of the app I created some successful and unsuccessful scenarios and registered it with the debug tool, both, front and backend. 

**Some of the validations added to the constructor**

![Constructor](tut-react-and-spring-data-rest/basic/images/Debug constructor.png)

**Failed scenario with null attribute**

![NullEx](tut-react-and-spring-data-rest/basic/images/debug database.png)

**Frontend debug tool firefox**

![frontEndDebug](tut-react-and-spring-data-rest/basic/images/debug frontend.png)

### 1.1.4 First assignment - Adding changes to repository

All these updates were pushed to the repo with numerous commits and solved issues involved in it , concluding the first part of the assignment with the tag 1.2.0. 

## **1.2.1 Second assignment - Create branch - "email-task" branch** 

In order to create a new branch I used the following commands:

>**git branch email-task**

>**git checkout email-task**

### **1.2.2 Second assignment - Implement email field**

Bellow the preview of the app with email field added. 

**Backend**

![EmailfieldBack](tut-react-and-spring-data-rest/basic/images/debug constructor com email.png)

**Preview**

![EmailField](tut-react-and-spring-data-rest/basic/images/mail field.png)

### **1.2.3 Second assignment - Support tests and debug**

**Email Debug Backend**

![EmailDebugBack](tut-react-and-spring-data-rest/basic/images/email debug back.png)


**Email Debug Frontend**

![EmailDebugFront](tut-react-and-spring-data-rest/basic/images/debugfrontendemail.png)

### **1.2.4 Second assignment - Creating branch for fixing bugs**

At this point I didn't knew but I proceeded to the creation of another branch without merging the first one. However , more updfront this issue will be solved. It was actually a very instructive mistake. 

The following commands were used to create another branch:

> **git branch fix-invalid-email**

> **git checkout fix-invalid-email**

### **1.2.5 Second assignment - Implementing solution**

For this the method emailIsValid was created. 
In there, I validate the email by adding the obligatory use of the "@" in the String inserted. Also, it was all supported by unit tests. 

### **1.2.5 Second assignement - Merging branches with master**

After committing and pushing all the updates its time to merge branches. 
First, to check our branches I used:

> **git branch -v**

Then, we go back to the master:

> **git checkout master**

finnaly we merge the branch into the master:

> **git merge fix-invalid-email**

> **git push**

After checking the git log with:

> **git log** 

I noticed that i didn't merge the previous email-task and proceeded to do it. 
To end this assignment I updated the tags and pushed all the latest updated files including the Readme.

> **git add .**
>
> **git commit -m "Conclusion Readme"**

I've also added the final tags with the tag ***ca1-final***:

> **git tag -a ca1-final -m "final tag  marking repository"**

### 2. Analysis of an Alternative

At this point , the next task was to find an alternative to git. 
After some research , I chose Mercurial as an alternative DVCS, Distributed Version Control Systems. 
Mercurial is a free , open-source version management tool capable of handling any kind of project. It uses a central repository and, like git, a local copy of it too. 

### Git vs Mercurial 

* Easier to use - One of the major difference between the Mercurial tool and the Git tool is the ease of use of the tool. The mercurial tool is a comparatively easy to use tool compared to the Git tool.
* Flexibility - Git is more flexible compared to Mercurial. There are many features offered by Git that are not present in Mercurial. Git is also  as it offers a low-level command to the user.
* Staging area - The staging area is where the user can check wich files are going to be in the following commit. From these two , Git is the only software capable of that. 
* Branching - In this case, branching in Mercurial seems to be much more complex. Because of that , Git branching mechanism, is much stronger , solid and easier to use. 
* Support - One of the main differences when using both softwares it is noticeable the superiority of Git when searching for solutions and information.

After using both of them , I think they're quite similar and both very effective on their job. 
Mercurial seems to be more appropriate for inexperienced users as it has fewer functionalities hence it's easier for beginners.
Also, it is important to mention that , having a GUI makes work more pleasant and fluid. 
 

> ** Bellow one of the images I used as reference **

![GitVsMerc](tut-react-and-spring-data-rest/basic/images/gitvsmerc.png)

### 3. Implementing Alternative - Mercurial using TortoiseHG
