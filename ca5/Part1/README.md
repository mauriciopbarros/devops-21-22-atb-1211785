# Class Assignment 5 Report - Part 1

The goal of this first part of the Class Assignment is to practice with Jenkins using the "gradle basic demo" in my repository.

## 1. Analysis, Design and Implementation

#### 1.1 Install and Setup JENKINS

First I started by installing Jenkins on my computer. 
Since i am using Linux Fedora i used the command: 

>sudo dnf install jenkins

Then with the command bellow, io enbled and started Jenkins: 

> /etc/init.d/jenkins start
Instead of creating an account, since ive already experimented with that(and to be honest forgot my credentials) I disable Jenkins login through the edition of the config file(with VIM). 
By changing the **<useSecurity>true</useSecurity> to --> <useSecurity>false</useSecurity>** i was able to enter my profile without having to log in. 

![img1](../Part1/Screenshots/JENKINSFILESEC.png)

Jenkins status: 

![img2](../Part1/Screenshots/jenkinsstatus.png)

Jenkins Profile: 

![img3](../Part1/Screenshots/jenkinsProf.png)

#### 1.2 Create Pipeline using Jenkins

The first step was to add an item and there create a Pipeline, which I named "ca5_Part1", 
as can be seen in the image below

![img4](../Part1/Screenshots/pipelinecrea.png)

Then it was time to add the script to create the pipeline.
First the script had the following configuration, 

SCRIPT : 

![img5](../Part1/Screenshots/SCRIPTJENKINS.png)

BUILD : 
![img6](../Part1/Screenshots/buildgreen.png)


After the build passed i also tried the same pipeline , but this time, through the directory of its Jenkinsfile. 
For that , in the general settings tab of my Jenkins pipeline, after inserting the URL for my repository i made the pipeline script from SCM. I chose Git as my SCM. In order to get my credentials in a correct way i created a new ssh key at bitbucket and used it to allow access to my repo through the jenkins file. 

![img7](../Part1/Screenshots/pipelineOPTION2.png)

![img7](../Part1/Screenshots/buildgreen225.png)