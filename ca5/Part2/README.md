# **Class Assignment 5 Report - Part 2**

# **CI/CD Pipelines with Jenkins**

The goal of this second part of the Class Assignment is to practice with Jenkins using the tutorial spring boot application, gradle "basic" version, in my repository - developed in Ca21-Part2.

## **1. Analysis, Design and Implementation**

For this new  assignment which was focused on the spring boot application of the CA2-part2 we had to create a new Jenkinsfile as will be seen below.

I had to had new stages which had different purposes, and they were  the following ones:

* ***Javadoc*** : Generates the javadoc of the project and publish it in Jenkins. ( use publishHTML step to archive/publish html reports);

* ***Publish Image*** : Generate a docker image with Tomcat and the war file and publish it in the Docker Hub.  
                        Use docker.build to build an image from a Dockerfile in the same folder as the Jenkinsfile. The tag for the image will be the job build number of Jenkins.

Jenkinsfile : 

![Jenkinns](../Part2/Screenshots/jenkinsfile.png)

As can be seen above in the stage for Javadoc I was using the publishHTML which was generated using Pipeline Syntax. I had this plugin (*HTML publisher Plugin*) but also the *Javadoc plugin* in Jenkins installed.

![publishHTML](../Part2/Screenshots/pipelinesyntax.png)

In the *'Archiving' stage* as the war file was needed and had to be archived, in order for this to happen I had to add in the build.gradle file the war plugging: 

            plugins {
                id 'war'
            }

 ##### **1.2 Setting the Dockerfile**


For the Image to be created it was also needed a Dockerfile, in this case only for the web application:

> FROM tomcat:8-jdk8-temurin
>
> COPY build/libs/basic-0.0.1-SNAPSHOT.war usr/local/tomcat/webapps/
>
> EXPOSE 8080

##### **1.1.3 Running the Jenkins Pipeline CA5_Part2**


After having everything settled it was time to run the Jenkins pipeline making sure the Docker Desktop was running locally.

Unfortunately , since I am using fedora, i had several problems with my docker app. The fact that the default container app for fedora is podman created many dificulties that i wasnt able to solve in time. 