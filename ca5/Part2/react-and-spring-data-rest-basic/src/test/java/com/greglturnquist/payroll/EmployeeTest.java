package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class EmployeeTest {


    @Test
    public void employeeCreationFailToNullInputFirstName() {
        // Arrange
        String firstName = null;
        String lastName = "Junior";
        String description = "Worker";
        String jobTitle = "Boss";
        int jobYears = 0;
        String email = "invalidEmail@mIdDlLeEaRtH.ORC";

        // Assert
        assertThrows(NullPointerException.class, () -> new Employee(firstName,
                lastName, description, jobTitle, jobYears, email));
    }

    @Test
    public void employeeCreationFailToNullInputLastName() {
        // Arrange
        String firstName = "Juvenal";
        String lastName = null;
        String description = "Worker";
        String jobTitle = "Boss";
        int jobYears = 0;
        String email = "invalidEmail@mIdDlLeEaRtH.ORC";

        // Assert
        assertThrows(NullPointerException.class, () -> new Employee(firstName,
                lastName, description, jobTitle, jobYears, email));
    }

    @Test
    public void employeeCreationFailToNullInputDescription() {
        // Arrange
        String firstName = "Juvenal";
        String lastName = "Junior";
        String description = null;
        String jobTitle = "Boss";
        int jobYears = 0;
        String email = "invalidEmail@mIdDlLeEaRtH.ORC";

        // Assert
        assertThrows(NullPointerException.class, () -> new Employee(firstName,
                lastName, description, jobTitle, jobYears, email));
    }

    @Test
    public void employeeCreationFailToNullInputJobTitle() {
        // Arrange
        String firstName = "Juvenal";
        String lastName = "Junior";
        String description = "Worker";
        String jobTitle = null;
        int jobYears = 0;
        String email = "invalidEmail@mIdDlLeEaRtH.ORC";

        // Assert
        assertThrows(NullPointerException.class, () -> new Employee(firstName,
                lastName, description, jobTitle, jobYears, email));
    }

    @Test
    public void employeeCreationFailToNegativeJobYears() {
        // Arrange
        String firstName = "Juvenal";
        String lastName = "Junior";
        String description = "Worker";
        String jobTitle = "Boss";
        int jobYears = -1;
        String email = "invalidEmail@mIdDlLeEaRtH.ORC";

        // Assert
        assertThrows(NullPointerException.class, () -> new Employee(firstName,
                lastName, description, jobTitle, jobYears, email));
    }

    @Test
    public void employeeCreationFailToNullEmail() {
        // Arrange
        String firstName = "Juvenal";
        String lastName = "Junior";
        String description = "Worker";
        String jobTitle = "Boss";
        int jobYears = -1;
        String email = null;

        // Assert
        assertThrows(NullPointerException.class, () -> new Employee(firstName,
                lastName, description, jobTitle, jobYears, email));
    }

    @Test
    public void employeeCreationFailInvalidEmailFormat() {
        // Arrange
        String firstName = "Juvenal";
        String lastName = "Junior";
        String description = "Worker";
        String jobTitle = "Boss";
        int jobYears = -1;
        String email = "asd.ds.w";

        // Assert
        assertThrows(NullPointerException.class, () -> new Employee(firstName,
                lastName, description, jobTitle, jobYears, email));
    }

    @Test
    public void employeeCreationSuccessfully() {
        // Arrange
        String firstName = "Juvenal";
        String lastName = "Junior";
        String description = "Worker";
        String jobTitle = "Boss";
        int jobYears = 1;
        String email = "invalidEmail@mIdDlLeEaRtH.ORC";

        // Act
        Employee employee = new Employee(firstName, lastName, description, jobTitle,
                jobYears, email);

        // Assert
        assertEquals(firstName, employee.getFirstName());
        assertEquals(lastName, employee.getLastName());
        assertEquals(description, employee.getDescription());
        assertEquals(jobTitle, employee.getJobTitle());
        assertEquals(jobYears, employee.getJobYears());
        assertEquals(email, employee.getEmail());
    }


}